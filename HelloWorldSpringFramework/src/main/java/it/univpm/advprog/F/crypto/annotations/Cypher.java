package it.univpm.advprog.F.crypto.annotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("renderer")
public class Cypher implements MessageRenderer {
	MessageProvider provider;
	int shift;
	
	public Cypher() {
		this(10);
	}
	
	public Cypher(int shift) {
		this.shift = shift;
	}
	
	@Override
	public void render() {
		if (this.provider == null) {
			throw new RuntimeException("Provider mancante");
		}
		
		String cleartext = this.provider.getMessage();
		
		char[] encrypted = new char[cleartext.length()];
		
		for (int i=0; i<cleartext.length(); i++)
		{
			char c = cleartext.charAt(i);
			encrypted[i] = (char)(c + this.shift);
		}
		
		System.out.print("< ");
		System.out.println(encrypted);
	}

	// TODO esercizio: se ometto questo @Autowired appare un'eccezione molto criptica; debuggare + scoprire il motivo
	@Override
	@Autowired
	public void setMessageProvider(MessageProvider provider) {
		this.provider = provider;		
	}

	@Override
	public MessageProvider getMessageProvider() {
		return this.provider;
	}

	
	
}
