package it.univpm.advprog.F.crypto.annotations;

public interface MessageProvider {
	String getMessage();
}
