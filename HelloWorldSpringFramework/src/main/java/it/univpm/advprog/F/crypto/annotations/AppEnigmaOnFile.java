package it.univpm.advprog.F.crypto.annotations;

import org.springframework.context.support.GenericXmlApplicationContext;

public class AppEnigmaOnFile {
	public static void main(String ...args) {

		GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:applicationContext-crypto-annotations.xml");
        ctx.refresh();
	    
	    MessageRenderer mr = ctx.getBean("renderer-file", MessageRenderer.class);
		
	    while (true) {
	    	mr.render();
	    }
	}
}
