package it.univpm.advprog.F.crypto.annotations;

import org.springframework.context.support.GenericXmlApplicationContext;

public class AppEnigma {
	public static void main(String ...args) 
	{
		try (GenericXmlApplicationContext ctx = new GenericXmlApplicationContext()) 
		{
	        ctx.load("classpath:applicationContext-crypto-annotations.xml");
	        ctx.refresh();
		   	    
		    MessageRenderer mr = ctx.getBean("renderer", MessageRenderer.class);
			
		    while (true) {
		    	mr.render();
		    }
		}
	}
}
