package it.univpm.advprog.E.crypto;

public class Cypher implements MessageRenderer {
	MessageProvider provider;
	int shift;
	
	public Cypher(int shift) {
		this.shift = shift;
	}
	
	@Override
	public void render() {
		if (this.provider == null) {
			throw new RuntimeException("Dipendenza mancante");
			
		}
		
		String cleartext = this.provider.getMessage();
		
		char[] encrypted = new char[cleartext.length()];
		
		for (int i=0; i<cleartext.length(); i++)
		{
			char c = cleartext.charAt(i);
			encrypted[i] = (char)(c + this.shift);
		}
		
		System.out.print("< ");
		System.out.println(encrypted);
	}

	@Override
	public void setMessageProvider(MessageProvider provider) {
		this.provider = provider;		
	}

	@Override
	public MessageProvider getMessageProvider() {
		return this.provider;
	}

	
	
}
