package it.univpm.advprog.E.crypto;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppEnigma {
	public static void main(String ...args) {
		try (ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext-crypto.xml")) {
			
		    MessageRenderer mr = context.getBean("renderer", MessageRenderer.class);
			
		    while (true) {
		    	mr.render();
		    }
		}
	}
}
