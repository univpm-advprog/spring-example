package it.univpm.advprog.E.crypto;

import java.io.FileWriter;
import java.io.IOException;

public class CypherOnFile implements MessageRenderer {
	MessageProvider provider;
	int shift;
	String filename;
	
	public CypherOnFile(int shift, String filename) {
		this.shift = shift;
		this.filename = filename;
	}
	
	@Override
	public void render() {
		if (this.provider == null) {
			throw new RuntimeException("Dipendenza mancante");
			
		}

		
		String cleartext = this.provider.getMessage();
		
		char[] encrypted = new char[cleartext.length()];
		
		for (int i=0; i<cleartext.length(); i++)
		{
			char c = cleartext.charAt(i);
			encrypted[i] = (char)(c + this.shift);
		}
		
		// open in append mode
		try (FileWriter writer = new FileWriter(this.filename, true)) {
			writer.append(new String(encrypted) + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setMessageProvider(MessageProvider provider) {
		this.provider = provider;		
	}

	@Override
	public MessageProvider getMessageProvider() {
		return this.provider;
	}

	
	
}
