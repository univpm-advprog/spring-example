package it.univpm.advprog.E.crypto;

public interface MessageProvider {
	String getMessage();
}
