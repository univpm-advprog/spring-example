package it.univpm.advprog.A.basic;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppHello {
	

	public static void main(String[] args) {
		// loading the definitions from the given XML file
		
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext-basic.xml");
		HelloWorldService serviceMario = (HelloWorldService)context.getBean("helloMario"); // dependency lookup with cast
		
		String message = serviceMario.sayHello();
		System.out.println(message);
		
		// change bean
		HelloWorldService serviceGianna = context.getBean("helloGianna", HelloWorldService.class); // dependency lookup with generic types
		message = serviceGianna.sayHello();
		System.out.println(message);
		
		serviceGianna.setName(serviceMario.sayHello());
		message = serviceGianna.sayHello();
		System.out.println(message);
		
		// get the same bean twice (after a modification)
		serviceGianna = context.getBean("helloGianna", HelloWorldService.class); // dependency lookup with generic types
		message = serviceGianna.sayHello();
		System.out.println(message);
			
		
		((ClassPathXmlApplicationContext)context).close();
		
	}
}
