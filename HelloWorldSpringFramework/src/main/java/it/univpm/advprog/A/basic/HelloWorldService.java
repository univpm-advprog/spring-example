package it.univpm.advprog.A.basic;

import org.springframework.stereotype.Service;

@Service("helloWorldService")
public class HelloWorldService {
	private String name;
	
	public HelloWorldService() {
		System.out.println("HelloWorldService created!");
	}
	
	public void setName(String name) {
		this.name = name;
		
	}
	
	public String sayHello() {
		return "Hello! " + this.name;
		
	}
}
