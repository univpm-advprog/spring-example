package it.univpm.advprog.G.crypto.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("it.univpm.advprog.G")
public class MyConfiguration {
	@Bean
	public MessageProvider provider() {
		return new InputProvider();
	}
	

	@Bean
	public int shift() {
		return 10;
	}
	
	@Bean
	public String secretFileName() {
		return "secret-config.txt";
	} 
}
