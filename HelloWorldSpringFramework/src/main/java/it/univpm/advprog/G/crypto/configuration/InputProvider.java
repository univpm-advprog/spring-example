package it.univpm.advprog.G.crypto.configuration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.springframework.stereotype.Component;

@Component("provider")
public class InputProvider implements MessageProvider {
	BufferedReader buffin ;
	
	public InputProvider() {
		this(System.in);
	}

	public InputProvider(InputStream instream) {
		Reader inreader = new InputStreamReader(instream);
		this.buffin = new BufferedReader(inreader);
	}
	
	
	public String getMessage() {
		try {
			System.out.print("> ");
			return this.buffin.readLine();
		} catch (IOException e) {
			e.printStackTrace();
			return "<Error: " + e.getMessage() + ">";
		}
	}

}
