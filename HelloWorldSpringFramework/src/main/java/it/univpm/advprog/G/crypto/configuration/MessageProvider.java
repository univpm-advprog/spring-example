package it.univpm.advprog.G.crypto.configuration;

public interface MessageProvider {
	String getMessage();
}
