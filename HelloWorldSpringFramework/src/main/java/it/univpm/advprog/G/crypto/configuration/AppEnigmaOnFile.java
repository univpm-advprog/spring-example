package it.univpm.advprog.G.crypto.configuration;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppEnigmaOnFile {
	public static void main(String ...args) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(MyConfiguration.class);
	    
	    MessageRenderer mr = ctx.getBean(CypherOnFile.class);
		
	    while (true) {
	    	mr.render();
	    }
	}
}
