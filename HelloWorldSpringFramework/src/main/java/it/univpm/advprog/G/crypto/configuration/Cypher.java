package it.univpm.advprog.G.crypto.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Cypher implements MessageRenderer {
	MessageProvider provider;
	int shift;
	/*
	 * non piu` necessario
	public Cypher() {
		this(10);
	} */
	
	public Cypher(@Value("10") int shift) {
		this.shift = shift;
	}
	
	@Override
	public void render() {
		String cleartext = this.provider.getMessage();
		
		char[] encrypted = new char[cleartext.length()];
		
		for (int i=0; i<cleartext.length(); i++)
		{
			char c = cleartext.charAt(i);
			encrypted[i] = (char)(c + this.shift);
		}
		
		System.out.print("< ");
		System.out.println(encrypted);
	}

	@Override
	@Autowired
	public void setMessageProvider(MessageProvider provider) {
		this.provider = provider;		
	}

	@Override
	public MessageProvider getMessageProvider() {
		return this.provider;
	}

	
	
}
