package it.univpm.advprog.G.crypto.configuration;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppEnigma {
	public static void main(String ...args) {

		try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(MyConfiguration.class)) 
		{
	    
		    MessageRenderer mr = ctx.getBean(Cypher.class);
			
		    while (true) {
		    	mr.render();
		    }
		}
	}
}
