package it.univpm.advprog.D.decoupled.xml;

import it.univpm.advprog.E.crypto.MessageProvider;
import it.univpm.advprog.E.crypto.MessageRenderer;

public class StdOutMessageRenderer implements MessageRenderer {
	private MessageProvider provider;
	
	public void render() {
		if (this.provider == null) {
			throw new RuntimeException("You must inject the dependency!");
		}
		System.out.println(this.provider.getMessage());
	}

	public void setMessageProvider(MessageProvider provider) {
		this.provider = provider;

	}

	public MessageProvider getMessageProvider() {
		return this.provider;
	}

}
