package it.univpm.advprog.D.decoupled.xml;

import it.univpm.advprog.E.crypto.MessageProvider;

public class StringProvider implements MessageProvider {
	String content ;
	
	public StringProvider() {
		this.content = "Hello world!";
	}
	
	public StringProvider(String content) {
		this.content = content;		
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getContent() {
		return this.content;
	}
	
	public String getMessage() {
		return this.content;
	}

}
