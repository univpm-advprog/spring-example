package it.univpm.advprog.D.decoupled.xml;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import it.univpm.advprog.E.crypto.MessageProvider;
import it.univpm.advprog.E.crypto.MessageRenderer;

public class AppHelloWorldDecoupledV1 {
	public static void main(String ...args) {

		try (ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext-decoupled-v1.xml")) {
	    
		    MessageProvider mpT = context.getBean("providerTopolino", MessageProvider.class);
		    StringProvider mpP = context.getBean("providerPluto", StringProvider.class);
		    MessageRenderer mr = context.getBean("renderer", MessageRenderer.class);
	
		    mr.setMessageProvider(mpT);
			mr.render();
			
			mr.setMessageProvider(mpP);
			mr.render();
			
			mpP.setContent(mpT.getMessage());
			mr.render();
			
		}
	}
}
