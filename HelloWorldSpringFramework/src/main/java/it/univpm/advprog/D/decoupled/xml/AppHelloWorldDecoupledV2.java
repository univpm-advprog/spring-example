package it.univpm.advprog.D.decoupled.xml;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import it.univpm.advprog.E.crypto.MessageRenderer;

public class AppHelloWorldDecoupledV2 {
	public static void main(String ...args) {

		try (ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext-decoupled-v2.xml")) {
	    
		    MessageRenderer mr = context.getBean("renderer", MessageRenderer.class);
			mr.render();
		
		}
	}
}
