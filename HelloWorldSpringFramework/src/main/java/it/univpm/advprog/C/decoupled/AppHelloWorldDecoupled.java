package it.univpm.advprog.C.decoupled;

public class AppHelloWorldDecoupled {
	
	public static void main(String ...args) {
		
		MessageProvider mp;
		
		if (args.length > 0) {
			mp = new StringProvider(String.join(" ", args));
		} else {
			mp = new StringProvider("Ciao Zio Paperone!");
		}
		
		MessageRenderer mr = new StdOutMessageRenderer();
		
		mr.setMessageProvider(mp);
		
		mr.render();
		
	}
}
