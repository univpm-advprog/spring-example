package it.univpm.advprog.C.decoupled;

public class StdOutMessageRenderer implements MessageRenderer {
	private MessageProvider provider;
	
	public void render() {
		if (this.provider == null) {
			throw new RuntimeException("You must inject the dependency!");
		}
		System.out.println(this.provider.getMessage());
	}

	public void setMessageProvider(MessageProvider provider) {
		this.provider = provider;

	}

	public MessageProvider getMessageProvider() {
		return this.provider;
	}

}
