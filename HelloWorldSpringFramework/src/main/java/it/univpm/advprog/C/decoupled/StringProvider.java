package it.univpm.advprog.C.decoupled;

public class StringProvider implements MessageProvider {
	String content ;
	
	public StringProvider() {
		this.content = "Hello world!";
	}
	
	public StringProvider(String content) {
		this.content = content;		
	}
	
	public String getMessage() {
		return this.content;
	}

}
