package it.univpm.advprog.C.decoupled;

public interface MessageProvider {
	String getMessage();
}
