package it.univpm.advprog.I.combined;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppCypheredCalculator {
	public static void main(String ...args) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(CalcCypherConfiguration.class);
	    
		// Nomi di bean possibili:
		// "calculator"
		// "cypher"
		// "cypherOnFile"
		String beanName = "calculator";
		
		if (args.length > 0) {
			beanName = args[0];
		}
		
	    MessageRenderer mr = ctx.getBean(beanName, MessageRenderer.class);
		
	    while (true) {
	    	mr.render();
	    }
	}
}
