package it.univpm.advprog.I.combined;

public interface MessageProvider {
	String getMessage();
}
