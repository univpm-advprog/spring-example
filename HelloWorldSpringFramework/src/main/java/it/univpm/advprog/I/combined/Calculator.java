package it.univpm.advprog.I.combined;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.udojava.evalex.Expression;

/*
 * Pipeline di bean
 *
 * InputProvider -> Calculator -> { Cypherer, CyphererOnFile }
 * 
 */

@Service("calculator")
public class Calculator implements MessageRenderer, MessageProvider {
	MessageProvider provider;
	int shift;
	
	@Autowired
	public Calculator(InputProvider provider) {
		this.provider = provider;
	}
	
	@Override
	public void render() {
		String output_text = this.getMessage();
		
		System.out.println("< " + output_text);
	}

	@Override
	public void setMessageProvider(MessageProvider provider) {
		this.provider = provider;		
	}

	@Override
	public MessageProvider getMessageProvider() {
		return this.provider;
	}

	@Override
	public String getMessage() {
		String user_text = this.provider.getMessage();
		
		Expression exp = new Expression(user_text);
		
		String result_text = "";
		try {
			BigDecimal result = exp.eval();
			result_text = result.toString();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			result_text = "<Error: " + e.toString() + ">";
		}
		
		return user_text + " = " + result_text;
	}

	
	
}
