package it.univpm.advprog.I.combined;

import java.io.FileWriter;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("cypherOnFile")
public class CypherOnFile implements MessageRenderer {
	MessageProvider provider;
	int shift;
	String filename;
	
	/* non necessario
	public CypherOnFile() {
		this(10, "secret.txt");
	} */
	
	@Autowired
	public CypherOnFile(int shift, String secretFileName, Calculator provider) {
		this.shift = shift;
		this.filename = secretFileName;
		this.provider = provider;
	}
	
	@Override
	public void render() {
		String cleartext = this.provider.getMessage();
		
		char[] encrypted = new char[cleartext.length()];
		
		for (int i=0; i<cleartext.length(); i++)
		{
			char c = cleartext.charAt(i);
			encrypted[i] = (char)(c + this.shift);
		}
		
		// open in append mode
		try (FileWriter writer = new FileWriter(this.filename, true)) {
			writer.append(new String(encrypted) + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	@Autowired
	public void setMessageProvider(MessageProvider provider) {
		this.provider = provider;		
	}

	@Override
	public MessageProvider getMessageProvider() {
		return this.provider;
	}

	
	
}
