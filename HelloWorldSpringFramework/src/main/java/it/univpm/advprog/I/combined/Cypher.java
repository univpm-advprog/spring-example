package it.univpm.advprog.I.combined;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("cypher")
public class Cypher implements MessageRenderer {
	MessageProvider provider;
	int shift;
	
	/*
	public Cypher() {
		this(10);
	} */
	
	@Autowired
	public Cypher(int shift, Calculator provider) {
		this.shift = shift;
		this.provider = provider;
	}
	
	@Override
	public void render() {
		String cleartext = this.provider.getMessage();
		
		char[] encrypted = new char[cleartext.length()];
		
		for (int i=0; i<cleartext.length(); i++)
		{
			char c = cleartext.charAt(i);
			encrypted[i] = (char)(c + this.shift);
		}
		
		System.out.print("< ");
		System.out.println(encrypted);
	}

	@Override
	public void setMessageProvider(MessageProvider provider) {
		this.provider = provider;		
	}

	@Override
	public MessageProvider getMessageProvider() {
		return this.provider;
	}

	
	
}
