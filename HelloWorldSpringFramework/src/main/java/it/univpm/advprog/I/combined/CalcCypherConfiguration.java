package it.univpm.advprog.I.combined;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("it.univpm.advprog.I")

public class CalcCypherConfiguration {
	/*
	@Bean
	public MessageProvider provider() {
		return new InputProvider();
	}
	
	@Bean
	public MessageRenderer renderer() {
		return new Calculator();
	}
	
	@Bean
	public MessageRenderer renderer_file() {
		return new CalculatorOnFile("calculator-configured.txt");
	}
	 */

	@Bean
	public int shift() {
		return 10;
	}
	
	@Bean
	public String calculatorFilename() {
		return "calculator-configured.txt";
	}
}
