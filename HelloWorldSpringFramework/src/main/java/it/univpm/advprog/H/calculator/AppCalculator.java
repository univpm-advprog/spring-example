package it.univpm.advprog.H.calculator;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppCalculator {
	public static void main(String ...args) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(CalcConfiguration.class);
	    
		String beanName = "renderer";
		
		if (args.length > 0) {
			beanName = args[0];
		}
		
	    MessageRenderer mr = ctx.getBean(beanName, MessageRenderer.class);
		
	    while (true) {
	    	mr.render();
	    }
	    

	}
}
