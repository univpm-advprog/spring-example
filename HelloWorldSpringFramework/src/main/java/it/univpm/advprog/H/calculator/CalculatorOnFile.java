package it.univpm.advprog.H.calculator;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.udojava.evalex.Expression;

// mostra errore se duplico nome di @Service
@Service("renderer_file")
public class CalculatorOnFile implements MessageRenderer {
	MessageProvider provider;
	String filename;
	
	/*
	// aggiunto per evitare errori dovuti a mancanza di costruttore di default quando si usano annotazioni; 
	// in futuro vedremo come specificare valori di parametri del costruttore
	public CalculatorOnFile() {
		this("calculator-default.txt");
	} */
	
	@Autowired
	public CalculatorOnFile(String calculatorFilename) {
		this.filename = calculatorFilename;
	}
	
	@Override
	public void render() {
		String user_text = this.provider.getMessage();
		
		Expression exp = new Expression(user_text);
	
		String result_text = "";
		try {
			BigDecimal result = exp.eval();
			result_text = result.toString();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			result_text = "<Error: " + e.toString() + ">";
		}
		
		String line = user_text + " = " + result_text + "\n";
		
		// open in append mode
		try (FileWriter writer = new FileWriter(this.filename, true)) {
			writer.append(line);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	@Autowired
	public void setMessageProvider(MessageProvider provider) {
		this.provider = provider;		
	}

	@Override
	public MessageProvider getMessageProvider() {
		return this.provider;
	}

	
	
}
