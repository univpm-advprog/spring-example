package it.univpm.advprog.H.calculator;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.udojava.evalex.Expression;

@Service("renderer")
public class Calculator implements MessageRenderer {
	MessageProvider provider;
//	
//	public Calculator() {
//		
//	}
	
	@Override
	public void render() {
		String user_text = this.provider.getMessage();
		
		Expression exp = new Expression(user_text);
		
		String result_text = "";
		try {
			BigDecimal result = exp.eval();
			result_text = result.toString();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			result_text = "<Error: " + e.toString() + ">";
		}
		
		System.out.println("< " + user_text + " = " + result_text);
	}

	@Override
	@Autowired
	public void setMessageProvider(MessageProvider provider) {
		this.provider = provider;		
	}

	@Override
	public MessageProvider getMessageProvider() {
		return this.provider;
	}

	
	
}
