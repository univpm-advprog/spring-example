package it.univpm.advprog.H.calculator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("it.univpm.advprog.H")

public class CalcConfiguration {
	/*
	 * 
	 * le inizializzazioni di @Bean qui commentate sono alternative a quelle che si ottengono tramite
	 * @Service(...) in testa alla definizione delle singole classi
	@Bean
	public MessageProvider provider() {
		return new InputProvider();
	}
	
	@Bean
	public MessageRenderer renderer() {
		return new Calculator();
	}
	
	@Bean
	public MessageRenderer renderer_file() {
		return new CalculatorOnFile("calculator-configured.txt");
	}
	*/

	@Bean
	public String calculatorFilename() {
		return "calculator-configured.txt";
	}
}
