package it.univpm.advprog.H.calculator;

public interface MessageProvider {
	String getMessage();
}
